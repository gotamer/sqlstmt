package sqlstmt

/*

const (
	SQLPP_USER_NAME_UNIQUE uint = iota
	SQLPP_USER_INSERT
)

var (
	db        *sql.DB
	StmtCache stmt.DBStmts
)

func init() {
	var err error
	dbname := "some.db"
	if db, err = sql.Open("sqlite3", dbname); err != nil {
		log.Fatal("sql open: ", err)
	}
	StmtCache = stmt.Open(10)
}

func final() {
	errs := StmtCache.Close()
	for _, err := range errs {
		log.Println("sql.stmt.Close(): ", err)
	}
	if err := db.Close(); err != nil {
		log.Println("sql.Close(): ", err)
	}
}

func sqlStmt(st *stmt.DBStmt) *stmt.DBStmt {
	var ok bool
	st, ok = StmtCache.Get(st)
	if ok != true {
		st = sqlStmtFromDB(st)
		StmtCache.Add(st)
	}
	return st
}

func sqlStmtFromDB(stmt *stmt.DBStmt) *stmt.DBStmt {
	var err error
	switch stmt.Id {
	case SQLPP_USER_NAME_UNIQUE:
		stmt.St, err = db.Prepare("SELECT username FROM users WHERE LOWER(username) = LOWER(:name)")
	case SQLPP_USER_INSERT:
		stmt.St, err = stmt.Tx.Prepare("INSERT INTO users(username,password,email,groups) VALUES(?,?,?,?)")
	default:
		fmt.Println("Unknown Sql Statment")
	}
	if err != nil {
		log.Printf("Failed to prepare statement id: %d Error: %s", stmt.Id, err.Error())
	}
	return stmt
}

func sqlUserAdd(username, password, email, groups string) (id int64) {

	var err error
	var result sql.Result

	sc := StmtCache.New(SQLPP_USER_INSERT)

	sc.Tx, err = db.Begin()
	if err != nil {
		log.Fatalln("sql Begin: ", err)
	}

	sc = sqlStmt(sc)

	result, err = sc.St.Exec(username, password, email, groups)
	if err != nil {
		log.Fatalln("sql Exec: ", err)
	}

	id, err = result.LastInsertId()
	if err != nil {
		log.Fatalln("sql lastId: ", err)
	}

	sc.Tx.Commit()
	return
}

*/
