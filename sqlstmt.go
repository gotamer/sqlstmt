// sql/database statement cache
// A must for Ajax Autocomplete!
// bitbucket.org/gotamer/stmt provides a cache for database/sql statements.
// The cache is time duration based and provides a means to specify max items to cache.
// Latest used items will stay in the cache, old items will be removed keeping
// the specified max items in the cache.
// Check doc.go for an example
// Code is considered ALPHA
package sqlstmt

// This does not quite work with transaction statements.
// The item gets commited but following error happens:
// sql Exec:  sql: Transaction has already been committed or rolled back

import (
	"database/sql"
	"sort"
	"sync"
	"time"
)

// Structure to hold sql stmt info
type DBStmt struct {
	Mx sync.Mutex
	Id uint
	Tx *sql.Tx
	St *sql.Stmt
	tm time.Duration
}

// Needed for Sort function
type stmts []*DBStmt

type DBStmts struct {
	Mx  sync.Mutex
	max int
	ts  time.Time
	ss  stmts
}

// Open creates a new instance
func Open(max int) DBStmts {
	var s DBStmts
	s.Mx.Lock()
	defer s.Mx.Unlock()
	s.ts = time.Now()
	s.max = max
	return s
}

// Should be closed before closing the database.
// Wish there was a sister to init() called final()
// Since there ins't it is the callers resposibility to call this before
// application termination.
func (ds *DBStmts) Close() (errs []error) {
	for _, stmt := range ds.ss {
		if err := stmt.St.Close(); err == nil {
			ds.ss = stmts{}
		} else {
			errs = append(errs, err)
		}
	}
	ds = &DBStmts{}
	return
}

// Convinence helper to create a new DBStmt type
func (ds *DBStmts) New(id uint) (s *DBStmt) {
	s = new(DBStmt)
	s.Id = id
	return
}

// Add adds a new DBStmt to the cache
// Also removes old once if the capacity is reached
func (ds *DBStmts) Add(s *DBStmt) {
	id := len(ds.ss)
	max := ds.max - 1
	if id >= max {
		sort.Sort(ds.ss)
		closeme := ds.ss[max:]
		ds.ss = ds.ss[0:max]
		for _, stmt := range closeme {
			stmt.St.Close()
		}
	}
	s.tm = time.Now().Sub(ds.ts)
	ds.ss = append(ds.ss, s)
}

// Has is not really needed use Get()
func (ds *DBStmts) Has(id uint) bool {
	for _, s := range ds.ss {
		if s.Id == id {
			return true
		}
	}
	return false
}

// Has and Get in one, bool true means it has
func (ds *DBStmts) Get(stmt *DBStmt) (*DBStmt, bool) {
	if len(ds.ss) != 0 {
		for _, s := range ds.ss {
			if s.Id == stmt.Id {
				stmt.St = s.St
				stmt.Tx = s.Tx
				stmt.tm = time.Now().Sub(ds.ts)
				return stmt, true
			}
		}
	}
	return stmt, false
}

// Sort interface Len
func (s stmts) Len() int {
	return len(s)
}

// Sort interface Less
func (s stmts) Less(i, j int) bool {
	return s[i].tm > s[j].tm
}

// Sort interface Swap
func (s stmts) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}
