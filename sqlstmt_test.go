package sqlstmt

import (
	"testing"
)

var (
	StmtCache DBStmts
)

func TestS(t *testing.T) {
	var ok bool
	max := 10
	StmtCache = Open(max)
	for i := uint(1); i < uint(max*3); i++ {
		sc := StmtCache.New(i)
		if sc.Id != i {
			t.Errorf("Id should be: %d, but is: %d", i, sc.Id)
		}
		StmtCache.Add(sc)
		if ok = StmtCache.Has(i); !ok {
			t.Errorf("Has should be: 'true', but is: %v", ok)
		}
		l := len(StmtCache.ss)
		if l > max {
			t.Errorf("Len should be: %d, but is: %d", max, l)
		}
	}
}
